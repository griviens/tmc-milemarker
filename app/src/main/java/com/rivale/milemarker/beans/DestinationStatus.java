package com.rivale.milemarker.beans;

/**
 * Created by Ale on 01/03/2016.
 */
public class DestinationStatus {

    private Destination destination;
    private Integer remaining;
    private Trend trend = Trend.EQUAL;

    public DestinationStatus(Destination destination){
        this.destination = destination;
    }

    public Destination getDestination() {
        return destination;
    }

    public Integer getRemaining() {
        return remaining;
    }

    public void setRemaining(Integer remaining) {
        if(this.remaining == null)trend = Trend.DECREASED;
        else{
            if(remaining.longValue() < this.remaining.longValue())trend = Trend.DECREASED;
            else trend = Trend.INCREASED;
        }
        this.remaining = remaining;
    }

    public Trend getTrend() {
        return trend;
    }
}
