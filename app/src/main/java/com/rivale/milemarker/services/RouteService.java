package com.rivale.milemarker.services;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.rivale.milemarker.R;
import com.rivale.milemarker.beans.Destination;
import com.rivale.milemarker.beans.DestinationStatus;
import com.rivale.milemarker.beans.Trend;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ale on 01/03/16.
 */
public class RouteService {

    public static RouteService routeService = null;
    public static Context mContext;
    public static String google_directions_key;
    public static List<Destination> destinations;
    public static List<DestinationStatus> destinationsStatuses;
    public static String summary;
    public static String previousSummary;
    public static DestinationStatus previousDestinationStatus;

    XmlPullParserFactory factory;
    XmlPullParser xpp;

    public RouteService(Context mContext){
        init(mContext);
    }

    private void init(Context mContext) {

        this.mContext = mContext;
        this.google_directions_key = mContext.getResources().getString(R.string.google_directions_key);

        try {
            this.factory = XmlPullParserFactory.newInstance();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        this.factory.setNamespaceAware(true);
        this.destinations = new ArrayList<>();
        this.destinationsStatuses = new ArrayList<>();
    }

    private List<Destination> loadDestinations(String summary) {
        List<Destination> destinations = new ArrayList<>();
        try {
            String list[] = mContext.getAssets().list("gpx");
            for (String s : list) {
                if(s.toLowerCase().contains(summary.toLowerCase())) {
                    destinations.add(new Destination(getLastPoint(s), s));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return destinations;
    }

    private LatLng getLastPoint(String s) {

        LatLng lastPoint = null;

        try {
            xpp = factory.newPullParser();
            File f = new File(s);
            xpp.setInput(mContext.getAssets().open("gpx/"+f.getPath()),"UTF-8");
            int eventType = xpp.getEventType();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if(eventType == XmlPullParser.START_TAG) {
                    if(xpp.getName().equals("wpt")){

                        String lat = xpp.getAttributeValue(0);
                        String lon = xpp.getAttributeValue(1);

                        lastPoint = new LatLng(Double.valueOf(lat),Double.valueOf(lon));
                    }
                }
                eventType = xpp.next();
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IndexOutOfBoundsException e){
            e.printStackTrace();
        }
        return lastPoint;
    }

    public static RouteService getInstance(Context mContext){
        if(routeService == null){
            routeService = new RouteService(mContext);
        }
        return routeService;
    }

    public static String baseUrl = "https://maps.googleapis.com/maps/api/directions/json?";

    private JSONObject getMapResult(String lat1,String lon1) throws JSONException {
        return getMapResult(lat1,lon1,lat1,lon1);
    }
    private JSONObject getMapResult(String lat1,String lon1, String lat2, String lon2) throws JSONException {
        //https://maps.googleapis.com/maps/api/directions/json?origin=&destination=&mode=driving&key=AIzaSyBbpDugK1DOl7n4MCfc3M2Xg
        String url = baseUrl + "origin="+lat1+","+lon1+"&destination="+lat2+","+lon2+"&mode=drive&key=" + google_directions_key;
        String res = protectedCall(url);
        return new JSONObject(res);
    }

    public String updateDrivingStatus(JSONObject coords) {

        try {
            String currentLat = coords.getString("lat");
            String currentLong = coords.getString("lon");

            JSONObject jRes = getMapResult(currentLat, currentLong);
            JSONObject jRoute = getJRoute(jRes);

            //prendo summary
            String currentSummary = jRoute.getString("summary");
            if(currentSummary != null && !currentSummary.equals("")){
                if(!currentSummary.equals(summary)){
                    Log.d("Summary","different! reload dest for:"+currentSummary);
                    destinations = loadDestinations(currentSummary);
                }
                previousSummary = summary;
                summary = currentSummary;
            }

            LatLng currentPosition = new LatLng(Double.valueOf(currentLat),Double.valueOf(currentLong));

            updateDistances(currentPosition);
            DestinationStatus destinationStatus = this.getDrivingDirection();

            if(destinationStatus == null) {
                if (previousDestinationStatus != null) {
                    destinationStatus = previousDestinationStatus;
                    return "you are driving on "+summary+" and probably your direction is "+destinationStatus.getDestination().getName().replace(".gpx","");
                }
                return "you are driving on "+summary+" and.. still dont know your destination";
            }
            else{
                previousDestinationStatus = destinationStatus;
                return "you are driving on "+summary+" and probably your direction is "+destinationStatus.getDestination().getName().replace(".gpx","");
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return "something went wrong.... maybe google api key :/";
        }
    }

    public JSONObject getJRoute(JSONObject jRes) throws JSONException {
        return jRes.getJSONArray("routes").getJSONObject(0);
    }

    private void updateDistances(LatLng current) throws JSONException {
        List<DestinationStatus> destinationStatusesNew = new ArrayList<>();

        String lat1 = current.latitude+"";
        String lon1 = current.longitude+"";

        for(Destination destination : destinations){
            DestinationStatus destinationStatus = getDestinationStatus(destination);
            String lat2 = destinationStatus.getDestination().getDestination().latitude+"";
            String lon2 = destinationStatus.getDestination().getDestination().longitude+"";
            JSONObject res = getMapResult(lat1,lon1,lat2,lon2);
            destinationStatus.setRemaining(getDistance(getJRoute(res)));
            destinationStatusesNew.add(destinationStatus);
        }

        destinationsStatuses = destinationStatusesNew;
    }

    private Integer getDistance(JSONObject jsonObject) throws JSONException {
        JSONArray legs = jsonObject.getJSONArray("legs");
        JSONObject leg = legs.getJSONObject(0);
        JSONObject distance = leg.getJSONObject("distance");
        return distance.getInt("value");
    }

    public String protectedCall(String callUrl){
        try {
            URL url = new URL(callUrl+"&key=");
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line).append("\n");
                }
                bufferedReader.close();
                return stringBuilder.toString();
            }
            finally{
                urlConnection.disconnect();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public DestinationStatus getDrivingDirection(){

        //if only one is decreased, then is the probable destination
        //if more than one are decreased, then the probable destination is the nearest
        int countDecreased = 0;
        DestinationStatus probable = null;
        for(DestinationStatus destinationStatus : destinationsStatuses){
            if(destinationStatus.getTrend().equals(Trend.DECREASED)){
                countDecreased++;
                probable = destinationStatus;
            }
        }

        if(countDecreased == 1)return probable;
        else{
            Integer min = null;
            for(DestinationStatus destinationStatus : destinationsStatuses){
                if(min == null){
                    min = destinationStatus.getRemaining();
                    probable = destinationStatus;
                }
                else{
                    if(destinationStatus.getRemaining() < min){
                        probable = destinationStatus;
                    }
                }
            }
            return probable;
        }
    }

    private DestinationStatus getDestinationStatus(Destination destination) {
        for(DestinationStatus destinationStatus : destinationsStatuses){
            if(destinationStatus.getDestination().equals(destination))return destinationStatus;
        }
        return new DestinationStatus(destination);
    }
}
